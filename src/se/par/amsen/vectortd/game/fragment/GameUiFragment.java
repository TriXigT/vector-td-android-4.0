package se.par.amsen.vectortd.game.fragment;

import se.par.amsen.vectortd.R;
import se.par.amsen.vectortd.fragment.framework.ControlledFragment;
import se.par.amsen.vectortd.fragment.framework.FragmentControllerService;
import se.par.amsen.vectortd.fragment.transition.animator.TransitionAccelerateOut;
import se.par.amsen.vectortd.fragment.transition.animator.TransitionBounceIn;
import se.par.amsen.vectortd.game.game.main.GameService;
import android.animation.AnimatorListenerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;

public class GameUiFragment extends ControlledFragment {
	private static final String TAG = "PauseFragment";

	private ImageButton btnPause;
	private ImageButton btnBuild;
	private Button btnWin;

	@Override
	public View init(LayoutInflater inflater, ViewGroup container) {
		View v = inflater.inflate(R.layout.fragment_game_main_ui, container, false);

		OptionsOnClickListener listener = new OptionsOnClickListener();

		btnPause = (ImageButton) v.findViewById(R.id.btn_game_ui_pause);
		btnBuild = (ImageButton) v.findViewById(R.id.btn_game_ui_build);
		btnWin = (Button) v.findViewById(R.id.btn_win_game);

		btnBuild.setOnClickListener(listener);
		btnPause.setOnClickListener(listener);
		btnWin.setOnClickListener(listener);

		return v;
	}

	@Override
	public void transitionInSetup(final AnimatorListenerAdapter listener) {

		setTransitionIn(new TransitionBounceIn(listener));
	}

	@Override
	public void transitionOut(final AnimatorListenerAdapter listener) {
		TransitionAccelerateOut transition = new TransitionAccelerateOut(listener);
		transition.load(getView()).start();
	}
	
	@Override
	public void onBackPressed() {
		btnPause.performClick();
	}

	private class OptionsOnClickListener implements OnClickListener {

		@Override
		public void onClick(View v) {
			if(FragmentControllerService.INSTANCE.getFragmentController().getFocus().equals(FragmentControllerService.INSTANCE.GAME_UI)) {
				int id = v.getId();

				if(id == btnBuild.getId()) {

				} else if(id == btnPause.getId()) {
					GameService.INSTANCE.pause();

					getFragmentController().pushWithTransition(FragmentControllerService.INSTANCE.GAME_PAUSE, null);
				} else if(id == btnWin.getId()) {
					getFragmentController().pushWithTransition(FragmentControllerService.INSTANCE.GAME_HIGH_SCORE, null);
				}
			}
		}
	}


}