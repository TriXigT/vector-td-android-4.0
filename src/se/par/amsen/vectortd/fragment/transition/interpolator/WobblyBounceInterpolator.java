package se.par.amsen.vectortd.fragment.transition.interpolator;

import android.animation.TimeInterpolator;

public class WobblyBounceInterpolator implements TimeInterpolator {

		@Override
		public float getInterpolation(float time) {
			double s = (time*5>1 ? 1 : time)+(Math.sin(6*time*Math.PI)/Math.exp(Math.PI*time));

			return (float) s;
		}

		
	}