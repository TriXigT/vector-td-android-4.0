package se.par.amsen.vectortd.font;

import android.content.Context;
import android.graphics.Typeface;

public class FontService {
	private Context context;
	private static FontService instance;

	public FontService(Context context) {
		this.context = context;
	}

	public static FontService getInstance(Context context) {
		synchronized (FontService.class) {
			if (instance == null) {
				instance = new FontService(context);
			}
			return instance;
		}
	}

	public Typeface getTypeFace() {
		return Typeface.createFromAsset(context.getResources().getAssets(), "fonts/BRLNSB.TTF");
	}
}
