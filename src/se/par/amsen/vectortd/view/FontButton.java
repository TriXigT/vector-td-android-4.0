package se.par.amsen.vectortd.view;

import se.par.amsen.vectortd.font.FontService;
import android.content.Context;
import android.util.AttributeSet;
import android.widget.Button;

public class FontButton extends Button {
	public FontButton(Context context) {
		super(context);
		init(context);
	}
	
	public FontButton(Context context, AttributeSet attrs) {
		super(context, attrs);
		init(context);
	}

	public FontButton(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		init(context);
	}

	public void init(Context context) {
		setTypeface(FontService.getInstance(context).getTypeFace());
	}
}
