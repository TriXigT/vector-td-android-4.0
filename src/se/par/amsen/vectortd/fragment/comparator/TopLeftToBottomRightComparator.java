package se.par.amsen.vectortd.fragment.comparator;

import java.util.Comparator;

import android.view.View;

public class TopLeftToBottomRightComparator implements Comparator<View> {
	@Override
	public int compare(View v1, View v2) {
		//sort from top left to bottom right
		return (v1.getTop() > v2.getTop()) || (!(v1.getTop() == v2.getTop()) && (v1.getRight() <= v2.getLeft())) ? -1 : 1;
	}	
}