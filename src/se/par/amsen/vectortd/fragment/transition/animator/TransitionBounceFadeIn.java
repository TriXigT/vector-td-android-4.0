package se.par.amsen.vectortd.fragment.transition.animator;

import java.util.Collections;
import java.util.List;

import se.par.amsen.vectortd.fragment.comparator.TopLeftToBottomRightComparator;
import se.par.amsen.vectortd.fragment.transition.interpolator.WobblyBounceInterpolator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ArgbEvaluator;
import android.animation.ObjectAnimator;
import android.view.View;
import android.view.ViewGroup;

public class TransitionBounceFadeIn extends TransitionAnimator{

	private static final String TAG = "TransitionBounceIn";
	private ObjectAnimator fadeIn;
	private int fadeColorResource;
	public TransitionBounceFadeIn(AnimatorListenerAdapter listener, int fadeColorResource) {
		super(listener);
		this.fadeColorResource = fadeColorResource;
	}

	@Override
	public TransitionAnimator load(View v) {
		fadeIn = ObjectAnimator.ofObject(v,
				"backgroundColor",
				new ArgbEvaluator(),
				v.getResources().getColor(android.R.color.transparent),
				v.getResources().getColor(fadeColorResource));
		fadeIn.setDuration(300);

		List<View> views = extractTransitionViews((ViewGroup) v);
		List<AnimatorSet> animators = getAnimators();

		Collections.sort(views, new TopLeftToBottomRightComparator());

		int delay = 0; 

		for (View view : views) {
			AnimatorSet anim = new AnimatorSet();
			view.setScaleX(0f); 
			view.setScaleY(0f);

			anim.playTogether(
					ObjectAnimator.ofFloat(view, View.SCALE_X, 1f), 
					ObjectAnimator.ofFloat(view, View.SCALE_Y, 1f));
			anim.setInterpolator(new WobblyBounceInterpolator());
			anim.setDuration(500);
			anim.setStartDelay(delay);
			delay += 40;
			animators.add(anim);
		}

		if(getListener() != null)
			animators.get(animators.size()-1).addListener(getListener());

		return this;
	}
	
	@Override
	public TransitionAnimator start() {
		fadeIn.start();
		for(AnimatorSet animator : getAnimators()) {
			animator.start();
		}

		return this;
	}

}
