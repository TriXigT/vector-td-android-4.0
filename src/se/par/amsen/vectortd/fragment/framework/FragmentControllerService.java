package se.par.amsen.vectortd.fragment.framework;

public enum FragmentControllerService {
	INSTANCE;

	public final String MAIN_MENU = "main_menu";
	public final String OPTIONS_MENU = "options_menu";
	public final String SCORES_MENU = "scores_menu";
	
	public final String GAME_PLAY = "game_play";
	
	public final String GAME_MAIN = "game_main";
	public final String GAME_PAUSE = "game_pause";
	public final String GAME_UI = "game_ui";
	public final String GAME_HIGH_SCORE = "game_high_score";
	public final String GAME_SMS = "game_sms";
	public final String GAME_NETWORK_CONNECT = "game_network_connect";
	
	private FragmentController controller = null;

	public void setFragmentController(FragmentController controller) {
		this.controller = controller;
	}
	
	public FragmentController getFragmentController() {
		return controller;
	}
}
