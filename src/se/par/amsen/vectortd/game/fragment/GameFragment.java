package se.par.amsen.vectortd.game.fragment;

import se.par.amsen.vectortd.fragment.framework.ControlledFragment;
import se.par.amsen.vectortd.fragment.framework.FragmentControllerService;
import se.par.amsen.vectortd.game.game.main.GameService;
import android.animation.AnimatorListenerAdapter;
import android.content.Context;
import android.graphics.Point;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

public class GameFragment extends ControlledFragment {
	private static final String TAG = "OptionsMenuFragment";

	Button btnBack;
	
	Context context;
	
	@Override
	public View init(LayoutInflater inflater, ViewGroup container) {
		Display display = getActivity().getWindowManager().getDefaultDisplay();
		Point size = new Point();
		display.getSize(size);
		
		GameService.INSTANCE.init(container.getContext(), size.x, size.y);
		
		View v = GameService.INSTANCE.getGameWorld().getRenderView();
		
		GameService.INSTANCE.start();
		
		GameService.INSTANCE.addUI();
		
		return v;
	}

	@Override
	public void transitionInSetup(AnimatorListenerAdapter listener) {
		listener.onAnimationEnd(null);
	}

	@Override
	public void transitionOut(AnimatorListenerAdapter listener) {
		listener.onAnimationEnd(null);
	}

	@Override
	public void onBackPressed() {
		GameService.INSTANCE.pause();

		getFragmentController().pushWithTransition(FragmentControllerService.INSTANCE.GAME_PAUSE, null);
	}
}
