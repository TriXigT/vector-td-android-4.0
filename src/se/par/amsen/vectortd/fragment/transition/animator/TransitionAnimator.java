package se.par.amsen.vectortd.fragment.transition.animator;

import java.util.ArrayList;
import java.util.List;

import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.view.View;
import android.view.ViewGroup;


public abstract class TransitionAnimator {
	private List<AnimatorSet> animators;
	private AnimatorListenerAdapter listener;

	public TransitionAnimator(AnimatorListenerAdapter listener) {
		this.listener = listener;
		animators = new ArrayList<AnimatorSet>();
	}

	public abstract TransitionAnimator load(View view);

	public TransitionAnimator start() {
		for(AnimatorSet animator : animators) {
			animator.start();
		}

		return this;
	}

	public List<View> extractTransitionViews(ViewGroup viewGroup) {
		List<ViewGroup> containers = findContainers(viewGroup);

		List<View> views = new ArrayList<View>();

		for(ViewGroup container : containers) {
			for(int i=0 ; i < container.getChildCount(); ++i) {
				if(container.getChildAt(i).getTag() == null || !container.getChildAt(i).getTag().equals("container")) {
					views.add(container.getChildAt(i));
				}
			}
		}

		return views;
	}

	public List<ViewGroup> findContainers(ViewGroup container) {
		List<ViewGroup> containers = new ArrayList<ViewGroup>();
		containers.add(container);

		return findContainers(containers);
	}

	private List<ViewGroup> findContainers(List<ViewGroup> containers) {
		List<ViewGroup> tempContainers = new ArrayList<ViewGroup>();

		for (ViewGroup viewGroup : containers) {
			for(int i=0 ; i < viewGroup.getChildCount(); ++i) {
				if(viewGroup.getChildAt(i).getTag() != null && viewGroup.getChildAt(i).getTag().toString().equals("container")) {
					ViewGroup view = (ViewGroup) viewGroup.getChildAt(i);
					tempContainers.add(view);
				}
			}
		}

		if(!tempContainers.isEmpty()) {
			tempContainers = findContainers(tempContainers);

			containers.addAll(tempContainers);
		}

		return containers;
	}

	public List<AnimatorSet> getAnimators() {
		return animators;
	}

	public void setAnimators(List<AnimatorSet> animators) {
		this.animators = animators;
	}

	public AnimatorListenerAdapter getListener() {
		return listener;
	}

	public void setListener(AnimatorListenerAdapter listener) {
		this.listener = listener;
	}

}
