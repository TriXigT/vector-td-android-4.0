package se.par.amsen.vectortd.fragment.transition.animator;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import se.par.amsen.vectortd.fragment.comparator.TopLeftToBottomRightComparator;

import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;

public class TransitionAccelerateOut extends TransitionAnimator{

	public TransitionAccelerateOut(AnimatorListenerAdapter listener) {
		super(listener);
	}

	@Override
	public TransitionAnimator load(View v) {
		List<View> views = new ArrayList<View>();
		List<AnimatorSet> animators = getAnimators();

		for(int i=0; i<((ViewGroup)v).getChildCount(); ++i) {
		    views.add(((ViewGroup)v).getChildAt(i));
		}
		
		Collections.sort(views, new TopLeftToBottomRightComparator());
		
		Collections.reverse(views);
		
		int delay = 0; 
		
		for (View view : views) {
			AnimatorSet anim = new AnimatorSet();
			view.setScaleX(1f); 
			view.setScaleY(1f);
			
			anim.playTogether(
					ObjectAnimator.ofFloat(view, View.SCALE_X, 0f), 
					ObjectAnimator.ofFloat(view, View.SCALE_Y, 0f));
			anim.setInterpolator(new AccelerateInterpolator());
			anim.setDuration(100);
			anim.setStartDelay(delay);
			delay += 10;
			animators.add(anim);
		}
		
		if(getListener() != null)
		animators.get(animators.size()-1).addListener(getListener());
		
		return this;
	}

}
