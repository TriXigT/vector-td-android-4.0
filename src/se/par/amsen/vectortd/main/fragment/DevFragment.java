package se.par.amsen.vectortd.main.fragment;

import se.par.amsen.vectortd.R;
import se.par.amsen.vectortd.fragment.framework.ControlledFragment;
import se.par.amsen.vectortd.fragment.framework.FragmentControllerService;
import se.par.amsen.vectortd.fragment.transition.animator.TransitionAccelerateOut;
import se.par.amsen.vectortd.fragment.transition.animator.TransitionBounceIn;
import android.animation.AnimatorListenerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;

public class DevFragment extends ControlledFragment {
	private static final String TAG = "MainMenuFragment";

	Button btnPlay;
	Button btnScores;
	Button btnOptions;

	Button btnGameUi;
	Button btnGamePause;
	Button btnGameHighScore;

	Button btnGameOver;
	Button btnGameSMS;

	@Override
	public View init(LayoutInflater inflater, ViewGroup container) {

		View v = inflater.inflate(R.layout.fragment_main_dev, container, false);

		btnPlay = (Button) v.findViewById(R.id.btn_play);
		btnScores = (Button) v.findViewById(R.id.btn_scores);
		btnOptions = (Button) v.findViewById(R.id.btn_options);

		btnGameUi = (Button) v.findViewById(R.id.btn_game_ui_overlay);
		btnGamePause = (Button) v.findViewById(R.id.btn_game_pause_menu);
		btnGameHighScore = (Button) v.findViewById(R.id.btn_high_score);

		//		btnGameOver = (Button) v.findViewById(R.id.btn_scores);
		btnGameSMS = (Button) v.findViewById(R.id.btn_sms);

		MainOnClickListener btnListener = new MainOnClickListener();

		btnPlay.setOnClickListener(btnListener);
		btnScores.setOnClickListener(btnListener);
		btnOptions.setOnClickListener(btnListener);

		btnGameUi.setOnClickListener(btnListener);
		btnGamePause.setOnClickListener(btnListener);
		btnGameHighScore.setOnClickListener(btnListener);

		//		btnGameOver.setOnClickListener(btnListener);
		btnGameSMS.setOnClickListener(btnListener);

		return v;
	}

	@Override
	public void transitionInSetup(AnimatorListenerAdapter listener) {
		setTransitionIn(new TransitionBounceIn(listener));
	}

	@Override
	public void transitionOut(AnimatorListenerAdapter listener) {		
		TransitionAccelerateOut transition = new TransitionAccelerateOut(listener);
		transition.load(getView()).start();
	}
	
	@Override
	public void onBackPressed() {
		//main activity exits...
	}

	private class MainOnClickListener implements OnClickListener {

		@Override
		public void onClick(View v) {
			if(FragmentControllerService.INSTANCE.getFragmentController().getFocus().equals(FragmentControllerService.INSTANCE.MAIN_MENU)) {
				int id = v.getId();
				if(id == btnPlay.getId()) {
					getFragmentController().swap(FragmentControllerService.INSTANCE.MAIN_MENU,
							FragmentControllerService.INSTANCE.GAME_PLAY, null);
				} 
				else if(id == btnScores.getId()) {
					getFragmentController().swap(FragmentControllerService.INSTANCE.MAIN_MENU,
							FragmentControllerService.INSTANCE.SCORES_MENU, null);
				} 
				else if(id == btnOptions.getId()) {
					getFragmentController().swap(FragmentControllerService.INSTANCE.MAIN_MENU,
							FragmentControllerService.INSTANCE.OPTIONS_MENU, null);
				} 
				else if(id == btnGameUi.getId()) {
					getFragmentController().pushWithTransition(FragmentControllerService.INSTANCE.GAME_UI, null);
				} 
				else if(id == btnGamePause.getId()) {
					getFragmentController().pushWithTransition(FragmentControllerService.INSTANCE.GAME_PAUSE, null);
				} 
				else if(id == btnGameHighScore.getId()) {
					getFragmentController().pushWithTransition(FragmentControllerService.INSTANCE.GAME_HIGH_SCORE, null);
				} 
				//			else if(id == btnGameOver.getId()) {
				//				getFragmentController().swap(FragmentControllerService.INSTANCE.MAIN_MENU,
				//						FragmentControllerService.INSTANCE.SCORES_MENU, null);
				//			} 
				else if(id == btnGameSMS.getId()) {
					getFragmentController().swap(FragmentControllerService.INSTANCE.MAIN_MENU,
							FragmentControllerService.INSTANCE.GAME_SMS, null);
				}
			}

		}
	}
}
