package se.par.amsen.vectortd.twitter;

import org.brickred.socialauth.AuthProvider;
import org.brickred.socialauth.android.DialogListener;
import org.brickred.socialauth.android.SocialAuthAdapter;
import org.brickred.socialauth.android.SocialAuthAdapter.Provider;
import org.brickred.socialauth.android.SocialAuthError;
import org.brickred.socialauth.util.AccessGrant;

import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.User;
import twitter4j.conf.ConfigurationBuilder;
import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;

public class TwitterService {

	private static final String TAG = "TwitterService";

	private SocialAuthAdapter adapter;
	private SharedPreferences prefs;
	private static Twitter twitter;

	private Activity activity;

	public void logInAndTweetSpam(Activity activity) {
		Log.v(TAG, "Attempting to log in and post tweet to twitter..");
		
		this.activity = activity;

		prefs = activity.getPreferences(Context.MODE_PRIVATE);

		if(prefs.contains("access_token") && prefs.contains("access_secret")){
			new LoginTask().execute(prefs.getString("access_token", ""), prefs.getString("access_secret", ""));

		}

		adapter = new SocialAuthAdapter(new DialogListener(){

			@Override
			public void onBack() { }

			@Override
			public void onCancel() { }

			@Override
			public void onComplete(Bundle arg0) {
				AuthProvider authProvider = adapter.getCurrentProvider();
				AccessGrant ag = authProvider.getAccessGrant();
				new LoginTask().execute(ag.getKey(), ag.getSecret()); 
				
			}

			@Override
			public void onError(SocialAuthError arg0) {
				prefs.edit().clear().commit();
				Log.v(TAG, "Cleared preferences..");

			}

		});

		adapter.authorize(activity, Provider.TWITTER);
	}

	public void tweetSpam() {

		LocationManager locationManager = (LocationManager) activity.getSystemService(Context.LOCATION_SERVICE);
		Location location = locationManager.getLastKnownLocation(locationManager.getBestProvider(new Criteria(), false));
		try {
			Log.v(TAG, "Attempting to tweet..");
			twitter.updateStatus(String.format("I am having so much fun with VectorTD at lat: %.2f, long: %.2f! Play this very much fun game now!", location.getLatitude(), location.getLongitude()));
		} catch (TwitterException e) {
			Log.w(null, "Could not spam to twitter, possible duplicate!");
			return;
		}
		
		Log.v(TAG, "Successfully tweeted..");
	}


	private class LoginTask extends AsyncTask<String, Void, Void> {

		@Override
		protected Void doInBackground(String... params) {
			Log.v(TAG, "Logging in to twitter..");
			
			String accessToken = params[0];
			String accessSecret = params[1];

			ConfigurationBuilder cb = new ConfigurationBuilder();
			cb.setDebugEnabled(true)
			.setOAuthConsumerKey("278IL7XdpkBvjN9Ktgmez34Gv")
			.setOAuthConsumerSecret("myuiel2761sSKmDWdUJ1dIz3UPHRDPlFVk2g0kg96LxwnQFYgB")
			.setOAuthAccessToken(accessToken)
			.setOAuthAccessTokenSecret(accessSecret);
			TwitterFactory tf = new TwitterFactory(cb.build());
			Twitter twitter = tf.getInstance();
			TwitterService.twitter = twitter;

			Log.v(TAG, "Succesfully logged in to twitter..");
			
			tweetSpam();

			try {
				User user = twitter.verifyCredentials();
				if(user!=null){
					Editor edit = prefs.edit();
					edit.putString("access_token", accessToken);
					edit.putString("access_secret", accessSecret);
					edit.commit();
				}

			} catch (TwitterException e) {
				Log.e(TAG, "Could not authorize");
			}
			return null;
		}
	}

}
