package se.par.amsen.vectortd.game.score;

public class ScoreItem {

	private int score;
	private String name;
	
	public ScoreItem(int score, String name) {
		this.score = score;
		this.name = name;
	}

	public int getScore() {
		return score;
	}
	
	public void setScore(int score) {
		this.score = score;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
