package se.par.amsen.vectortd.game.fragment;

import se.par.amsen.vectortd.R;
import se.par.amsen.vectortd.fragment.framework.ControlledFragment;
import se.par.amsen.vectortd.fragment.framework.FragmentControllerService;
import se.par.amsen.vectortd.fragment.transition.ControlledFragmentTransitionListenerAdapter;
import se.par.amsen.vectortd.fragment.transition.animator.TransitionAccelerateFadeOut;
import se.par.amsen.vectortd.fragment.transition.animator.TransitionBounceFadeIn;
import se.par.amsen.vectortd.game.game.main.GameService;
import se.par.amsen.vectortd.network.NetworkService;
import se.par.amsen.vectortd.twitter.TwitterService;
import android.animation.AnimatorListenerAdapter;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.Toast;

public class HighScoreFragment extends ControlledFragment {
	private static final String TAG = "HighScoreFragment";

	private ImageButton btnTweet;
	private ImageButton btnSms;
	private Button btnNewGame;
	private Button btnMainMenu;

	private TwitterService twitter;

	NetworkService network;

	@Override
	public View init(LayoutInflater inflater, ViewGroup container) {
		View v = inflater.inflate(R.layout.fragment_game_high_score, container, false);

		network = new NetworkService(getActivity());

		OptionsOnClickListener listener = new OptionsOnClickListener();

		btnTweet = (ImageButton) v.findViewById(R.id.btn_tweet);
		btnSms = (ImageButton) v.findViewById(R.id.btn_sms);
		btnNewGame = (Button) v.findViewById(R.id.btn_new_game);
		btnMainMenu = (Button) v.findViewById(R.id.btn_main_menu);

		btnTweet.setOnClickListener(listener);
		btnSms.setOnClickListener(listener);
		btnMainMenu.setOnClickListener(listener);
		btnNewGame.setOnClickListener(listener);

		twitter = new TwitterService();

		return v;
	}

	@Override
	public void transitionInSetup(final AnimatorListenerAdapter listener) {

		setTransitionIn(new TransitionBounceFadeIn(listener, R.color.blue_transp));
	}

	@Override
	public void transitionOut(final AnimatorListenerAdapter listener) {
		TransitionAccelerateFadeOut transition = new TransitionAccelerateFadeOut(listener, R.color.blue_transp);
		transition.load(getView()).start();
	}
	
	@Override
	public void onBackPressed() {
		btnMainMenu.performClick();
	}

	private class OptionsOnClickListener implements OnClickListener {

		@Override
		public void onClick(View v) {
			if(FragmentControllerService.INSTANCE.getFragmentController().getFocus().equals(FragmentControllerService.INSTANCE.GAME_HIGH_SCORE)) {
				int id = v.getId(); 
				if(id == btnTweet.getId()) {
					if(network.isConnectedToInternet()) {
						twitter.logInAndTweetSpam(getActivity());
						Toast toast = Toast.makeText(getActivity(), "Tweeting!", Toast.LENGTH_SHORT);
						toast.getView().setBackgroundResource(R.drawable.btn_red);
						toast.show();

					} else {
						new AlertDialog.Builder(getActivity()).
						setMessage("You are not connected to the internet! Click here to open network settings.")
						.setNegativeButton("Open settings", new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog, int which) {
								network.openWirelessSettings(); 
								Log.v(TAG, "Open network settings");
							}
						})
						.setPositiveButton("Close", null)
						.show();
					}
				} else if(id == btnSms.getId()) {
					getFragmentController().pushWithTransition(FragmentControllerService.INSTANCE.GAME_SMS, null);
				} else if(id == btnNewGame.getId()) {
					GameService.INSTANCE.reset();
					getFragmentController().removeAfterTransition(FragmentControllerService.INSTANCE.GAME_HIGH_SCORE, null);
				} else if(id == btnMainMenu.getId()) {
					getFragmentController().removeAfterTransition(FragmentControllerService.INSTANCE.GAME_HIGH_SCORE, new ControlledFragmentTransitionListenerAdapter() {					
						@Override
						public void onTransitionEnd() {
							getFragmentController().removeAfterTransition(FragmentControllerService.INSTANCE.GAME_UI,  new ControlledFragmentTransitionListenerAdapter() {
								@Override
								public void onTransitionEnd() {
									getFragmentController().swap(FragmentControllerService.INSTANCE.GAME_PLAY, FragmentControllerService.INSTANCE.MAIN_MENU,  null);
									GameService.INSTANCE.stop();
								}
							});

						}
					});
				}
			}
		}
	}


}